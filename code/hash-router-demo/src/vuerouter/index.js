let _Vue = null
export default class VueRouter {
  static install (Vue) {
    // 1 判断当前插件是否被安装
    if (VueRouter.install.installed) {
      return
    }
    VueRouter.install.installed = true
    // 2 把Vue的构造函数记录在全局
    _Vue = Vue
    // 3 把创建Vue的实例传入的router对象注入到Vue实例
    // _Vue.prototype.$router = this.$options.router
    _Vue.mixin({
      beforeCreate () {
        if (this.$options.router) {
          _Vue.prototype.$router = this.$options.router
        }
      }
    })
  }

  constructor (options) {
    this.options = options
    this.routeMap = {}
    this.isHistoryMode = options.mode && options.mode === 'history'
    // observable
    this.data = _Vue.observable({
      current: this.isHistoryMode ? window.location.pathname : window.location.hash.replace('#', '')
    })
    this.init()
  }

  init () {
    this.redirect()
    this.createRouteMap()
    this.initComponent(_Vue)
    this.initEvent()
  }

  redirect () {
    // 判断是hash模式，但url没有#的情况，重定向到相应带#的地址
    if (!this.isHistoryMode && window.location.hash === '') {
      window.location.replace('/#' + window.location.pathname)
    }
  }

  createRouteMap () {
    // 遍历所有的路由规则 把路由规则解析成键值对的形式存储到routeMap中
    this.options.routes.forEach(route => {
      this.routeMap[route.path] = route.component
    })
  }

  initComponent (Vue) {
    const self = this
    Vue.component('router-link', {
      props: {
        to: String
      },
      render (h) {
        const isCurrentActive = this.to === this.$router.data.current
        return h('a', {
          attrs: {
            href: (!self.isHistoryMode && '/#') + this.to,
            class: isCurrentActive ? 'router-link-exact-active router-link-active' : ''
          },
          on: {
            click: this.clickhander
          }
        }, [this.$slots.default])
      },
      methods: {
        clickhander (e) {
          history.pushState({}, '', self.isHistoryMode ? this.to : '/#' + this.to)
          this.$router.data.current = this.to
          e.preventDefault()
        }
      }
      // template:"<a :href='to'><slot></slot><>"
    })
    Vue.component('router-view', {
      render (h) {
        // self.data.current
        const cm = self.routeMap[self.data.current]
        return h(cm)
      }
    })
  }

  initEvent () {
    //
    if (this.isHistoryMode) {
      window.addEventListener('popstate', () => {
        this.data.current = window.location.pathname
      })
    } else {
      window.addEventListener('hashchange', () => {
        this.data.current = window.location.pathname
      }, false)
    }
  }
}
