# fed-e-task-03-01

## 简单题
#### 当我们点击按钮的时候动态给 data 增加的成员是否是响应式数据，如果不是的话，如何把新增成员设置成响应式数据，它的内部原理是什么。

  > 答案：不是。Vue 不允许在已经创建的实例上动态添加新的根级响应式属性 (root-level reactive property)。然而它可以使用 Vue.set(object, key, value) 方法将响应属性添加到嵌套的对象上。

  ``` javascript
  Vue.set(vm.dog, 'name', 'Trump')

  this.$set(this.dog, 'name', 'Trump')
  ```

  > 下面两行代码是核心，将新增属性设置为响应式，并手动触发通知该属性值的更新
  ``` javascript
  defineReactive(ob.value, key, val)
  ob.dep.notify()
  ```

  #### 请简述Diff算法的执行过程
  1. diff算法比较新旧节点的时候只会进行同层比较，不会跨层级比较
  <img src="images/2.jpg">

  2. diff算法的流程图
  <img src="images/1.jpg">

  - patch函数接收两个参数oldVnode和Vnode，分别代表新节点和旧节点
  - 通过sameVnode方法判断两个节点是否值得比较，值得比较的话就执行patchVnode，不值得就用Vnode直接替换oldVnode
  - 当我们确定两个节点值得比较后我们会对两个节点执行patchVnode，这个函数做了以下的事情：
    - 找到对应的真实dom，称为el
    - 判断Vnode和oldVnode是否指向同一个对象，如果是，那么直接return
    - 如果他们都有文本节点并且不相等，那么将el的文本节点设置为Vnode的文本节点。
    - 如果oldVnode有子节点而Vnode没有，则删除el的子节点
    - 如果oldVnode没有子节点而Vnode有，则将Vnode的子节点真实化之后添加到el
    - 如果两者都有子节点，则执行updateChildren函数比较子节点
  - 执行updateChildren函数会做以下两件事：
    - 将Vnode的子节点Vch和oldVnode的子节点oldCh提取出来
    - oldCh和vCh各有两个头尾的变量StartIdx和EndIdx，它们的2个变量相互比较，一共有4种比较方式。如果4种比较都没匹配，如果设置了key，就会用key进行比较，在比较的过程中，变量会往中间靠，一旦StartIdx>EndIdx表明oldCh和vCh至少有一个已经遍历完了，就会结束比较。
